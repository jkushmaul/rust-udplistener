# rust-udplistener

[![pipeline status](https://gitlab.com/jkushmaul/rust-udplistener/badges/master/pipeline.svg)](https://gitlab.com/jkushmaul/rust-udplistener/-/commits/master)

The most uncreative name in the world.

# Why

This all came about due to DTLS.   

To use Openssl rust's APIs, I need a single "stream" for each connected client.
If you are a client - no worries!  But if you are a server, you can only have one client
(Because you must "connect" the udp socket and that forces you to one peer.)

To have multiple peers, you need to "Route" datagrams in some way and this is what that does.

David Simmons' examples (https://github.com/simmons/tokio-aio-examples) were incredibly helpful on early issues.
I can't recall why I did not just use those other than pure stubbornness.

1. Because I don't know what I'm doing in rust.
2. I needed a stream-like impl of UdpSocket
3. I needed to have an async interface, with a sync interface for FFI.  I wanted to use message channels for that.

# No warranty

There is no session management - sessions are purely based on IP:PORT, they either exist, or do not exist and become routes on accept.

There is no state management - datagram A can arrive after datagram B.

This is all left to the user.

This will likely not work in any other environment than POC.

# Usage

It will become apparent that I just started rust development.

## First, create a stack:
```rust
let stack = UdpStack::bind("client", client_addr, mtu, backlog).await?;
```

## Stack creation

You can either:

A) Connect
```rust
    let (mut stream, join_result) = stack.connect(server_addr).await?;
```

XOR

B) Listen
```rust
    let Ok(mut stream) = listener.accept().await 
```

## Stream usage

You can

async send
```rust
    conn.send(buf).await 
```

async recv
```rust
    let Ok(datagram) = conn.recv().await
```

try send
```rust
    conn.try_send(buf) 
```

try recv
```rust
    let Ok(Some(datagram)) = conn.try_recv()
```

blocking_send
```rust
    conn.blocking_send(buf)
```

peek
```rust
    let Some(datagram) = conn.peek().await
```

try_peek
```rust
    let Some(datagram) = conn.try_peek()
```

# Future

* auto generate documentation instead of above
* Session management - use a timeout, with a GC.  Allow closing.
* Actual tests, like work.

That's all I have.

# Zero tests.

I'm lucky I even got this to compile.

# Example

See ./src/examples/main.rs for a client/server example.
