use std::io::{Result, ErrorKind};
use tokio::sync::mpsc::{Receiver};
use std::io;
use crate::udp_stack::UdpStackJoinResult;
use crate::udp_stream::UdpStream;

/*
Jesus tap dancing christ - I just want to "listen" on a port,
call "accept", and then have an isolated socket for that host + port.

let socket = UdpSocket::bind("127.0.0.1:31337").await?;
let mspc = Mspc::new();
let listener = UdpListener::new(socket, mspc);
let stack = UdpStack::new(socket, mspc);
let stack = task::spawn(stack.run(listener));

listener.accept().stream(|conn: UdpConnection| {
  conn.read().and_then( conn.write().await()).await();
});

 **/

#[derive(Debug)]
pub struct UdpListener {
    accept_rx: Receiver<UdpStream>,
    stack_join: UdpStackJoinResult,
}

impl UdpListener {
    pub fn new(accept_rx: Receiver<UdpStream>, stack_join: UdpStackJoinResult) -> Self {
        Self { accept_rx, stack_join}
    }


    pub async fn accept(&mut self) -> Result<UdpStream> {
        log::debug!("accepting udp");
        if let Some(conn) = self.accept_rx.recv().await {
            log::debug!("have a udp from accept_rx");
            Ok(conn)
        } else {
            log::debug!("received none from accept_rx, results in error");
            Err(io::Error::new(ErrorKind::ConnectionReset, "accept reset"))
        }
    }


    pub async fn stop(mut self)  {
        log::debug!("::stop() called, shutting down accept and stack");
        self.accept_rx.close();
        self.stack_join.join().await;
    }

}

