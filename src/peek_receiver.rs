use std::io::{Error, Result, ErrorKind};
use tokio::sync::mpsc::error::TryRecvError;
use tokio::sync::mpsc::Receiver;

#[derive(Debug)]
pub struct PeekReceiver<T> {
    rx: Receiver<T>,
    peeked: Option<T>,
}

impl<T> PeekReceiver<T> {
    pub fn new(rx: Receiver<T>) -> Self {
        Self {rx, peeked: None}
    }
    pub fn try_recv(&mut self) -> Result<Option<T>> {
        let peeked = self.peeked.take();
        if peeked.is_some() {
            return Ok(peeked);
        }

        match self.rx.try_recv() {
            Ok(s) => Ok(Some(s)),
            Err(e) => {
                match e {
                    TryRecvError::Empty => {
                        Ok(None)
                    }
                    TryRecvError::Disconnected => {
                        Err(Error::new(ErrorKind::ConnectionReset, "stack rx is closed"))
                    }
                }
            }
        }
    }

    pub async fn recv(&mut self) ->  Option<T> {
        let peeked = self.peeked.take();
        if peeked.is_some() {
            return peeked;
        }
        return self.rx.recv().await;
    }

    pub fn try_peek(&mut self) -> Result<Option<&T>> {
        if self.peeked.is_some() {
            return Ok(self.peeked.as_ref());
        }
        return match self.rx.try_recv() {
            Ok(p) => {
                self.peeked = Some(p);
                Ok(self.peeked.as_ref())
            }
            Err(TryRecvError::Empty) => Ok(None),
            Err(TryRecvError::Disconnected) => Err(Error::new(ErrorKind::ConnectionReset, "Disconnected"))
        };
    }

    pub async fn peek(&mut self) -> Result<Option<&T>> {
        if self.peeked.is_some() {
            return Ok(self.peeked.as_ref());
        }
        return match self.rx.recv().await {
            None => Err(Error::new(ErrorKind::ConnectionReset, "Disconnected")),
            Some(s) => {
                self.peeked = Some(s);
                Ok(self.peeked.as_ref())
            }
        };
    }









}