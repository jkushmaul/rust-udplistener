use std::io::{Error, ErrorKind};
use std::io::Result;
use std::net::SocketAddr;
use std::time::Instant;
use tokio::sync::mpsc::{Receiver, Sender};
use tokio::sync::mpsc::error::{TrySendError};
use crate::PeekReceiver;

#[derive(Debug)]
pub struct UdpDatagram {
    pub ts: Instant,
    pub sa_addr: SocketAddr,
    pub bytes: Vec<u8>,
}

impl UdpDatagram {
    pub fn new(sa_addr: SocketAddr, buf: &[u8]) -> Self {
        let bytes = Vec::from(buf);
        return Self {
            ts: Instant::now(),
            sa_addr,
            bytes
        };
    }
}




#[derive(Debug)]
pub struct UdpChannel {
    data_tx: Sender<UdpDatagram>,
    data_rx: PeekReceiver<UdpDatagram>,
}

#[derive(Debug)]
pub struct UdpStream {
    sa_addr: SocketAddr,
    udp_channel: UdpChannel
}

impl UdpStream {
    pub fn new(sa_addr: SocketAddr, data_tx: Sender<UdpDatagram>, data_rx: Receiver<UdpDatagram>) -> Self {
        let data_rx = PeekReceiver::new(data_rx);
        let udp_channel = UdpChannel {data_tx, data_rx};
        return Self{sa_addr, udp_channel};
    }

    pub fn get_addr(&self) -> &SocketAddr {
        return &self.sa_addr;
    }

    pub fn try_peek(&mut self) -> Result<Option<&UdpDatagram>> {
        self.udp_channel.data_rx.try_peek()
    }
    pub async fn peek(&mut self) -> Result<Option<&UdpDatagram>> {
        self.udp_channel.data_rx.peek().await
    }

    pub fn try_recv(&mut self) -> Result<Option<UdpDatagram>> {
        match self.udp_channel.data_rx.try_recv() {
            Ok(Some(dg)) => {
                let duration = Instant::now().duration_since(dg.ts).as_millis();
                log::debug!("recv() - received datagram of len {} with {}ms latency", dg.bytes.len(), duration);
                return Ok(Some(dg));
            },
            Ok(None) => Ok(None),
            Err(e) => Err(e)
        }
    }

    pub async fn recv(&mut self) -> Result<UdpDatagram> {
        if let Some(dg) = self.udp_channel.data_rx.recv().await {
            let duration = Instant::now().duration_since(dg.ts).as_millis();
            log::debug!("recv() - received datagram of len {} with {}ms latency", dg.bytes.len(), duration);
            return Ok(dg);
        }
        return Err(Error::new(ErrorKind::BrokenPipe, "channel was closed"));
    }

    pub fn blocking_send(&self, buf: &[u8]) -> Result<()> {
        let dg = UdpDatagram::new(self.sa_addr, buf);
        if let Err(e) = self.udp_channel.data_tx.blocking_send(dg) {
            return Err(Error::new(ErrorKind::ConnectionRefused, e));
        }
        return Ok(())
    }
    pub fn try_send(&self, buf: &[u8]) -> std::result::Result<(), TrySendError<UdpDatagram>> {
        let dg = UdpDatagram::new(self.sa_addr, buf);
        return self.udp_channel.data_tx.try_send(dg);
    }
    pub async fn send(&self, buf: &[u8]) -> Result<()> {
        let dg = UdpDatagram::new(self.sa_addr, buf);

        if let Err(e) = self.udp_channel.data_tx.send(dg).await {
            return Err(Error::new(ErrorKind::ConnectionRefused, e));
        }
        return Ok(());
    }
}
/*
impl AsyncRead for UdpStream {
    fn poll_read(mut self: Pin<&mut Self>, cx: &mut Context<'_>, buf: &mut ReadBuf<'_>) -> Poll<Result<()>> {
        let data_rx = &mut self.udp_channel.data_rx;
        return match data_rx.poll_recv(cx) {
            Poll::Ready(Some(d)) => {
                buf.put_slice(d.bytes.as_ref());
                Poll::Ready(Ok(()))
            },
            Poll::Ready(None) => {
                Poll::Pending
            }
            Poll::Pending => {
                Poll::Pending
            }
        }
    }
}

impl AsyncWrite for UdpStream {
    fn poll_write(self: Pin<&mut Self>, _cx: &mut Context<'_>, buf: &[u8]) -> Poll<Result<usize>> {
        let datagram = UdpDatagram::new(self.sa_addr, buf);
        if self.udp_channel.data_tx.is_closed() {
            return Poll::Ready(Err(io::Error::new(ErrorKind::ConnectionReset, "Reset")));
        }

        return match self.udp_channel.data_tx.try_send(datagram) {
            Ok(_) => {
                Poll::Ready(Ok(buf.len()))
            }
            Err(e) => { Poll::Ready(Err(io::Error::new(ErrorKind::ConnectionReset, e.to_string()))) }
        }
    }

    fn poll_flush(self: Pin<&mut Self>, _cx: &mut Context<'_>) -> Poll<Result<()>> {
        return Poll::Ready(Ok(()));
    }

    fn poll_shutdown(mut self: Pin<&mut Self>, _cx: &mut Context<'_>) -> Poll<Result<()>> {
        self.udp_channel.data_rx.close();
        return Poll::Ready(Ok(()));
    }
}*/