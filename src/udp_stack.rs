use std::net::SocketAddr;
use std::collections::{HashMap};
use std::io::{Result, ErrorKind, Error};
use tokio::sync::mpsc::{Sender};
use tokio::sync::mpsc::channel;
use tokio::net::UdpSocket;
use std::io;
use std::sync::Arc;
use std::time::Instant;
use tokio::runtime::{Runtime};
use tokio::task::JoinHandle;
use crate::UdpDatagram;
use crate::UdpListener;
use crate::UdpStream;
use crate::PeekReceiver;

/**
Shame shame - I think I heavily "borrowed" but Id on't remember where now -
Someone that implemented dtls already... That's the best I have.
It was open licensed and I will try to find it again.
**/


#[derive(Debug)]
struct UdpRoute {
    sa_addr: SocketAddr,
    data_tx: Sender<UdpDatagram>,
}

impl  UdpRoute {
    fn new(sa_addr: SocketAddr, data_tx: Sender<UdpDatagram>) -> Self {
        return Self{sa_addr, data_tx};
    }
}


#[derive(Debug)]
pub struct UdpStack {
    stack_name: String,
    socket: Arc<UdpSocket>,
    //this is cloned off to the udpstream, for it to send data through the stack.
    conn_tx:  Sender<UdpDatagram>,
    //this is the other end of that conn_tx, to be read within the stack.
    stack_rx: PeekReceiver<UdpDatagram>,
    routes: HashMap<SocketAddr, UdpRoute>,
    mtu: usize,
    max_packets: usize,
}

#[derive(Debug)]
pub struct UdpStackJoinResult {
    _rt: Runtime,
    tx: JoinHandle<Result<()>>,
    rx: JoinHandle<Result<()>>,
}
impl UdpStackJoinResult {
    pub async fn join(self) {
        match tokio::join!(
            self.tx,
            self.rx
        ) {
            _ => {}
        }
    }
}

impl UdpStack {

    pub fn new(stack_name: &str, socket: UdpSocket, mtu: usize, max_packets: usize) -> Self {
        let (conn_tx, stack_rx) = channel::<UdpDatagram>(max_packets);
        let routes = HashMap::new();
        let stack_name = stack_name.to_string();
        let stack_rx = PeekReceiver::new(stack_rx);
        Self { stack_name, socket: Arc::new(socket), conn_tx, stack_rx, mtu, routes, max_packets }
    }


    pub async fn bind(stack_name: &str, bind_addr: SocketAddr, mtu: usize, max_packets: usize) -> Result<Self> {
        Ok(UdpStack::new(stack_name, UdpSocket::bind(bind_addr).await?, mtu, max_packets))
    }

    pub async fn listen(self, backlog: usize) -> Result<UdpListener> {
        let (accept_tx, accept_rx) = channel::<UdpStream>(backlog);

        let join_result = self.forward(Some(accept_tx)).await?;
        let listener = UdpListener::new(accept_rx, join_result);
        Ok(listener)
    }

    pub async fn connect(mut self, sa_addr: SocketAddr) -> Result<(UdpStream, UdpStackJoinResult)> {
        self.socket.connect(sa_addr).await?;
        let stream = UdpStack::create_route(&mut self.routes, &self.stack_name, self.max_packets, &self.conn_tx, sa_addr).await?;
        log::debug!("{}::connect() - spawning forward", self.stack_name);

        let join_result = self.forward(None).await?;
        Ok((stream, join_result))
    }

    async fn forward(self, accept_tx: Option<Sender<UdpStream>>) -> Result<UdpStackJoinResult> {
        let stack_name = self.stack_name.clone();
        let stack_name2 = self.stack_name;

        let mtu = self.mtu;
        let max_packets = self.max_packets;
        let conn_tx = self.conn_tx;
        let mut stack_rx = self.stack_rx;
        let mut routes = self.routes;
        let rx_socket = self.socket.clone();
        let tx_socket = self.socket;

        let rt = Runtime::new()?;

        log::debug!("{}::forward() - running with buffer of {}", stack_name, mtu);
        let rx_join: JoinHandle<std::io::Result<()>> = rt.spawn(async move {

            let mut buf: Vec<u8> = vec![0u8; mtu];
            loop {
                let datagram = match rx_socket.recv_from(&mut buf).await {
                    Ok((sz, sa_addr)) => UdpDatagram::new(sa_addr, &buf[0..sz]),
                    Err(e) => {
                        log::debug!("{}::forward() - error in socket.recv_from: {}", stack_name, e);
                        return Err(e);
                    }
                };
                if !routes.contains_key(&datagram.sa_addr) {
                    if let Some(accept_tx) = accept_tx.as_ref() {
                        let conn = UdpStack::create_route(&mut routes, &stack_name, max_packets, &conn_tx, datagram.sa_addr).await?;
                        if let Err(e) = accept_tx.send(conn).await {
                            return Err(Error::new(ErrorKind::ConnectionRefused, e));
                        };
                        log::debug!("{}::ingress() - route created and sent to accept_tx", stack_name);
                    } else {
                        log::debug!("{}::ingress() - rejecting datagram, not accepted", stack_name);
                        return Ok(());
                    }
                }

                log::debug!("{}::forward() - have ingress from {} of length {}", stack_name, datagram.sa_addr, datagram.bytes.len());
                UdpStack::ingress(&mut routes, &stack_name, datagram).await?;
                continue;
            }
        });

        let tx_join: JoinHandle<std::io::Result<()>> = rt.spawn(async move {
            loop {
                let datagram = match stack_rx.recv().await {
                    Some(datagram) => datagram,
                    None => {
                        log::debug!("{}::forward() - stack_rx.recv was none", stack_name2);
                        return Ok(());
                    }
                };

                let duration = Instant::now().duration_since(datagram.ts).as_millis();
                log::debug!("{}::forward() - have egress of len {} with {}ms latency", stack_name2, datagram.bytes.len(), duration);
                match tx_socket.send_to(&datagram.bytes, datagram.sa_addr).await {
                    Ok(sz) => {
                        log::debug!("{}::egress() - socket.send_to() sent {} bytes to {}", stack_name2, sz, datagram.sa_addr);
                        continue;
                    },
                    Err(e) => {
                        log::debug!("{}::egress() - socket.send_to() in error to {}: {}", stack_name2, datagram.sa_addr, e);
                        return Err(e);
                    }
                }
            }
        });

        Ok(UdpStackJoinResult{_rt: rt, rx: rx_join, tx: tx_join})
    }



    async fn ingress(routes: &mut HashMap<SocketAddr, UdpRoute>, stack_name: &String, datagram: UdpDatagram)
                     -> Result<()> {

        let route = match routes.get_mut(&datagram.sa_addr) {
            None => {
                log::debug!("{}::ingress() - route n ot found for {}", stack_name, datagram.sa_addr);
                return Ok(())
            },
            Some(s) => s
        };
        if datagram.bytes.len() == 0 {
            return Ok(());
        }
        log::debug!("{}::ingress() - Buffering datagram from {} of length {}", stack_name, route.sa_addr, datagram.bytes.len());
        if route.data_tx.is_closed() {
            log::debug!("{}::ingress() - route.data_tx is closed for {}", stack_name, route.sa_addr);
            return Err(io::Error::new(ErrorKind::ConnectionReset, "conn reset"));
        }
        match route.data_tx.send(datagram).await {
            Ok(_) => Ok(()),
            Err(e) => {
                log::debug!("{}::ingress() - route.data_tx.send() for {}, in error: {}", stack_name, route.sa_addr, e);
                return Err(io::Error::new(ErrorKind::ConnectionReset, e));
            }
        }
        //cannot borrow `stack.routes` as mutable more than once at a time
        //if !result.is_ok() {
        //    stack.routes.remove(&route.sa_addr);
        //}
    }

    async fn create_route(routes: &mut HashMap<SocketAddr, UdpRoute>, stack_name: &String, max_packets: usize, conn_tx: &Sender<UdpDatagram>, sa_addr: SocketAddr) -> Result<UdpStream> {
        log::debug!("{}::create_route() - creating a route to {}", stack_name, sa_addr);
        let (route_tx, conn_rx) = channel::<UdpDatagram>(max_packets);
        let route = UdpRoute::new(sa_addr, route_tx);
        // a udp_connection is just a sock_addr, and the opposite ends of two pipes.
        let conn = UdpStream::new(sa_addr, conn_tx.clone(), conn_rx);

        routes.insert(sa_addr, route);
        return Ok(conn);
    }
}



