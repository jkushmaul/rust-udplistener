use bytes::BufMut;
use std::io::{Result};
use std::net::SocketAddr;
use tokio::io::{AsyncBufReadExt, BufReader};
use udp_listener::UdpStack;


#[tokio::main]
async fn main() -> Result<()> {
    env_logger::init();

    let server_addr: SocketAddr = "127.0.0.1:31337".parse().unwrap();

    let result = tokio::select! {
        result = run_server(server_addr) => result,
        result = run_client(server_addr) => result
    };

    result
}
async fn run_server(server_addr: SocketAddr) -> Result<()> {
    let mtu = 1500;
    let backlog = 4096;

    println!("Server - will bind to {}", server_addr);
    let stack = UdpStack::bind("server", server_addr, mtu, backlog).await?;
    println!("Server - listening");
    let mut listener = stack.listen(backlog).await?;


    println!("Server - accepting");
    while let Ok(mut conn) = listener.accept().await {
        println!("Server - accepted conn, saying hello first");
        let buf =
            "From Server - accept() - Hello, this is an echo server, I will repeat what you say\n.".as_bytes();
        match conn.send(buf).await {
            Ok(()) => {
                println!("Server - accept() - Wrote buf");
            }
            Err(e) => {
                println!("Server - accept() - Got error writing: {}", e);
            }
        }
        println!("Server - accept() - now echoing");
        loop {
            println!("Server - accept() - reading");
            match conn.recv().await {
                Ok(o) => {
                    let str = String::from_utf8_lossy(&o.bytes);
                    println!("Server - read {} bytes = '{}'", o.bytes.len(), str);
                    let mut v: Vec<u8> = Vec::from("You said: ".as_bytes());
                    v.put_slice(&o.bytes);
                    println!("Server - accept() - now writing");
                    match conn.send(v.as_slice()).await {
                        Ok(_) => {
                            println!("Server - write() - Wrote packet");
                        }
                        Err(e) => {
                            println!("Server - write() - Got error writing: {}", e);
                        }
                    }
                }
                Err(e) => {
                    println!("Server - read() - Got error reading: {}", e);
                }
            }
        }
    }
    println!("Server - accept() - Broke from loop");
    Ok(())
}

async fn run_client(server_addr: SocketAddr) -> Result<()> {
    let client_addr: SocketAddr = "127.0.0.1:31338".parse().unwrap();
    let mtu = 1500;
    let backlog = 4096;

    println!("Server - will bind to {}", client_addr);
    let stack = UdpStack::bind("client", client_addr, mtu, backlog).await?;
    println!("Client - connecting to {}", server_addr);
    let (mut client, join_result) = stack.connect(server_addr).await?;

    //let mut stringbuff = String::with_capacity(2048);
    // Lock our standard input to eliminate synchronization overhead (unlocks when dropped)

    println!("Client - Reading input");
    // Read our first line.

    let bufreader = BufReader::new(tokio::io::stdin());
    let mut lines = bufreader.lines();

    loop {
        match tokio::select! {
            result =  lines.next_line() => {
                println!("Client - next_line returned");
                let result = result?;
                if result.is_none() {
                    println!("Could not read line");
                    break;
                }
                let line = result.unwrap();

                println!("Client -  sending: '{}'...", line);
                client.send(line.as_bytes()).await?;
                println!("Client - sent: '{}'\n now receiving...", line);
            },
            result = client.recv() => {
                let result = result?;
                let bytebuff = result.bytes;
                let recvstr = String::from_utf8(Vec::from(bytebuff)).unwrap();
                println!("Client -  received: '{}'", recvstr);
            },
        } {
            _ => {}
        }
    }

    join_result.join().await;
    Ok(())
}
